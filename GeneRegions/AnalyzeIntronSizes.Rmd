Making gene region file to count overlapping reads
=========================================

```{r echo=FALSE}
species='Solanum lycopersicum'
annots_file='../ExternalData/S_lycopersicum_Sep_2019.bed.gz'
options(scipen=999)
```

Introduction
------------
  
To align sequences with a spliced aligner (e.g., tophat), we need to know the distribution of intron sizes. 

Questions:

* What is the largest intron in `r species`?
* What is the 99th percentile intron size?

Analysis
--------
  
Read the file with gene models and get columns we need to determine intron sizes:
  
```{r}
annots=read.delim(annots_file,header=F,as.is=T,sep='\t')
annots=annots[,c(4,10,11,12)]
names(annots)=c('transcript_id','num_exons','exon_lengths','exon_starts')
```

There were `r sum(annots$num_exons>1)` gene models (out of `r nrow(annots)`) with at least one intron.

Calculate intron sizes:

```{r}
getIntronSizes=function(rowOfData) {
  transcript_id = rowOfData[['transcript_id']]
  exon_lengths=as.numeric(unlist(strsplit(rowOfData[['exon_lengths']],
                                          split=",")))
  exon_starts=as.numeric(unlist(strsplit(rowOfData[['exon_starts']],
                                         split=",")))
  intron_starts=(exon_starts+exon_lengths)[1:(length(exon_starts)-1)]
  intron_ends=exon_starts[2:length(exon_starts)]
  intron_lengths=intron_ends-intron_starts
  #names(intron_lengths)=rep(transcript_id,length(intron_lengths))
  return(intron_lengths) 
}
intron_annots=annots[annots$num_exons>1,]
intron_lengths=apply(intron_annots,1,FUN=getIntronSizes)
names(intron_lengths)=intron_annots$transcript_id
```

Examine the distribution of intron sizes:
  
```{r fig.width=7,fig.height=5}
par(las=1)
main="Histogram of intron sizes"
ylab="base pairs"
intron_lengths=unlist(intron_lengths)
o=order(intron_lengths,decreasing = TRUE)
intron_lengths=intron_lengths[o]
hist(log10(intron_lengths))
q=quantile(intron_lengths,probs=seq(0,1,by=0.0001))
```

Results
-------

There were `r length(intron_lengths)` introns in the `r species` gene model annotations.

The largest intron was `r intron_lengths[1]` bases, from transcript 
`r names(intron_lengths)[1]`.

Visualization of this intron in the Integrated Genome Browser found that this intron spans a large region of moderately complex sequence, with no long stretches of "N" bases, which would indicate a gap in the assembly. 
The gene encodes a leucine-rich repeat containing gene with a realistic-looking translated region. 

The next largest intron was `r intron_lengths[2]` bases, from transcript
`r names(intron_lengths)[2]`. It also looks realistic in IGB. 

The 99.9th percentile was `r q["99.90%"]` bases. 

```{r}
boundary = round(q["99.90%"],-3)
```

Rounding to the nearest thousands place, this is: `r boundary`.

If we use this number as the maximum intron size parameter, how many
introns would we overlook?

```{r}
overlooked = intron_lengths[intron_lengths>boundary]
num_overlooked=length(overlooked)
num_genes_overlooked=length(unique(names(overlooked)))
```

If we choose this threshold, we will miss `r num_overlooked` introns in `r num_genes_overlooked` transcripts.

Conclusion
----------

If we use max intron size of `r boundary`, we will miss `r num_overlooked`
introns in the same number of genes. However, using this threshold 
will avoid incorrect merging of neighboring genes, which frequently occurs when the maximum intron size setting is large.

Let's use `r boundary` as the maximum intron size for `r species`.
