#!/usr/bin/env nextflow

params.dev = false
params.number_of_inputs = 2
params.saveMode = 'copy'
params.filePattern = "fastq/*_{R1,R2}_001.fastq.gz"
params.outdir = 'results'

Channel
    .fromFilePairs( params.filePattern )
    .ifEmpty { error "Cannot find any reads matching: ${params.filePattern}" }
    .take( params.dev ? params.number_of_inputs : -1 )
    .set { read_pairs_ch }
    
process trim {
    time '4h'
    memory '8 GB'
    cpus '1'
    input:
    tuple val(prefix), file(reads) from read_pairs_ch
    output:
    file '*.fq' into gzip_channel
    script:
    fq_1_paired = prefix + '_R1.p.fq'
    fq_1_unpaired = prefix + '_R1.u.fq'
    fq_2_paired = prefix + '_R2.p.fq'
    fq_2_unpaired = prefix + '_R2.u.fq'
    """
    trimmomatic \
    PE -phred33 \
    ${reads[0]} \
    ${reads[1]} \
    $fq_1_paired \
    $fq_1_unpaired \
    $fq_2_paired \
    $fq_2_unpaired \
    ILLUMINACLIP:TruSeq2-PE.fa:2:30:10 LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:50
    """
}

process gzip {
    time '1h'
    memory '8 GB'
    cpus '1'
    publishDir "$params.outdir", pattern: '*.fq.gz', mode: 'copy'
    input:
    file x from gzip_channel.flatten()
    output:
    file '*.fq.gz'
    script:
    """
    gzip -c $x > ${x}.gz
    """
}