#!/bin/bash
#PBS -q copperhead
#PBS -l nodes=1:ppn=8
#PBS -l mem=64gb
#PBS -l walltime=16:00:00
#
# Run like this:
#   qsub -N hisat2-build.sh -o hisat2-build.out -e hisat2-build.err hisat2-build.sh > histat2-build.job
# 
cd $PBS_O_WORKDIR
G=S_lycopersicum_Sep_2019
if [ ! -f twoBitToFa ]
then
    wget http://hgdownload.cse.ucsc.edu/admin/exe/linux.x86_64/twoBitToFa
    chmod a+x twoBitToFa
fi
B=$G.2bit
F=$G.fa
if [ ! -f $F ]
then 
    if [ ! -f $B ]
    then
	wget http://igbquickload.org/quickload/$G/$G.2bit
    fi
    ./twoBitToFa $B $F
fi
module load hisat2
hisat2-build -p 8 $F $G
