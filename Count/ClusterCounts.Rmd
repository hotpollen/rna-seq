---
title: "How do replicates cluster?"
author: "Ann Loraine"
date: "2/28/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Introduction

Before submitting sample data to SRA, we should check sample identity using clustering. (Sometimes samples get switched by accident.)

Also, we need to investigate sample similarity and potential batch effects before we can do differential expression analysis.

This R Markdown document asks:

* How do sample libraries cluster? 

# Results

Load required library and commonly used functions:

```{r message=FALSE, warning=FALSE, paged.print=FALSE}
library(edgeR)
source("../src/Common.R")
```

Read data:

```{r message=FALSE, warning=FALSE}
counts=getCounts()
```


Create data structure for subsequent analysis:

```{r message=FALSE, warning=FALSE, paged.print=FALSE}
sample_groups=sub(".[0-9]",names(counts),replacement="")
names(sample_groups)=names(counts)
big_DGEList=DGEList(counts=counts,group=sample_groups,
                  remove.zeros = TRUE)
```


Configure colors:

```{r}
sample_colors=getSampleColors()
```

Draw a barplot showing sequencing coverage by sample:

```{r}
main="Sequencing Depth per Sample Library"
ylab="Counts (millions)"
libsizes=big_DGEList$samples$lib.size/10**6
mindepth=round(min(libsizes))
maxdepth=round(max(libsizes))
names(libsizes)=rownames(big_DGEList$samples)
barplot(libsizes,col=sample_colors,las=2,main=main,ylab=ylab)
```

**Result**: 

* Sequencing depth was uniform across samples. 
* There were between `r mindepth` and `r maxdepth` million counts per sample. 

Use multi-dimensional scaling to examine sample relatedness:

```{r}
plotMDS(big_DGEList,col=sample_colors,main="MDS plot")
```

**Result**: 

* Samples cluster by variety.
* No separation by treatment evident at this scale.

Create function to cluster by variety:

```{r}
clusterByVariety = function(variety,counts) {
  varietyCode = substr(variety,1,1)
  sample_groups=sub(".[0-9]",names(counts),replacement="")
  indexes=grep(varietyCode,names(counts),value=T)
  sample_colors=getSampleColors()[indexes]
  groups=sample_groups[indexes]
  little_DGEList=DGEList(counts[,indexes],
                        group = groups,
                        remove.zeros = TRUE)
  plotMDS(little_DGEList,col=sample_colors,main=variety)
}
```

Cluster varieties separately:

```{r fig.height=8, fig.width=8}
varieties=getVarieties()
ncols=length(varieties)/2
nrows=length(varieties)/ncols
par(mfrow=c(nrows,ncols))
dge_lists=lapply(varieties,function(x){clusterByVariety(x,counts)})
```

**Result**:

* Replicate numbers cluster together. Were these samples paired? 

# Discussion

MDS clustering revealed that replicate numbers from treatment and control were close suggesting a batch effect.

To deal with this during differential expression testing, we can use an additive linear model that includes replicate and treatment effects.

# Conclusion

* Biological replicates cluster by variety
* Replicates numbers cluster near each other within variety